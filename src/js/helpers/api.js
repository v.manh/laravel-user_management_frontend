import axios from "axios";
const webapiDomain = "http://localhost:8000/api";
export function callApiDelete(apiSecret, CONTROLLER, params) {
    return axios
        .post(webapiDomain + "/cms/" + CONTROLLER + "/delete", params, {
            headers: {
                apisecret: apiSecret,
                "Content-Type":
                    "application/x-www-form-urlencoded;charset=UTF-8",
            },
        })
        .then((response) => {
            switch (response.status) {
                case 200:
                    return response.data;
                case 401:
                    // Xử lý khi session hết hạn
                    throw new Error("Session đã hết hạn!");
                case 501:
                    // Xử lý khi không có quyền thực hiện chức năng
                    throw new Error(
                        "Tài khoản không có quyền thực hiện chức năng này. Vui lòng liên hệ với Admin để được giải quyết"
                    );
                default:
                    // Xử lý lỗi mặc định
                    throw new Error("Xảy ra lỗi");
            }
        });
}

export function callApiModify(apiSecret, CONTROLLER, params) {
    return axios
        .post(webapiDomain + "/cms/" + CONTROLLER + "/new", params, {
            headers: {
                apisecret: apiSecret,
                "Content-Type":
                    "application/x-www-form-urlencoded;charset=UTF-8",
            },
        })
        .then((response) => {
            switch (response.status) {
                case 200:
                    return response.data;
                case 401:
                    // Xử lý khi session hết hạn
                    throw new Error("Session đã hết hạn!");
                case 501:
                    // Xử lý khi không có quyền thực hiện chức năng
                    throw new Error(
                        "Tài khoản không có quyền thực hiện chức năng này. Vui lòng liên hệ với Admin để được giải quyết"
                    );
                default:
                    // Xử lý lỗi mặc định
                    throw new Error("Xảy ra lỗi");
            }
        });
}

export async function callApiGetListUsers(params = null) {
    const headers = {
        "Content-Type": "application/x-www-form-urlencoded;charset=UTF-8",
    };
    const response = await axios.get(webapiDomain + "/users", {
        params,
        headers,
    });
    return response;
}
