import React from 'react';
import { Modal, Select, Input, DatePicker, Button } from 'antd';
import dayjs from 'dayjs';
import moment from 'moment';
import {callApiModify} from './api';
const { TextArea } = Input;

class PackageUpsalesModal extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            item: this.props.item
        };
    }

    handleChange(name, value) {
        if (this.props.controller == 'packageupsales') {
            let startDate = this.state.item.startDate ? this.state.item.startDate : ''
            this.setState(prevState => ({
                item: {
                ...prevState.item,
                startDate: startDate,
                [name]: value
              }
            }));
        } else {
            this.setState(prevState => ({
                item: {
                ...prevState.item,
                [name]: value
              }
            }));
        }
    }

    handleDateChange = (date, dateString) => {
        this.setState(prevState => ({
        item: {
            ...prevState.item,
            startDate: dateString ? moment(dateString).format('DD-MM-YYYY') : ''
        }
        }));
    };

    handleEditorChange = (event) => {
        if (this.props.controller != "packageupsales") {
            this.setState(prevState => ({
                item: {
                    ...prevState.item,
                    content: event.editor.getData(),
                }
            }));
        }
      };

    handleSaveModal = () => {
        let apiSecret = this.props.apiSecret ? this.props.apiSecret.apiSecret : ''
        if (!apiSecret) alert("Không có apiSecret")
        let params = this.state.item;
        if (this.props.controller == "packageupsales") {
            if (this.props.packageTypes) {
                let data = this.props.packageTypes.packageTypes;
                let value = this.state.item.packageType
                for (let i = 0; i < data.length; i++) {
                    if (data[i].code === value) {
                        params.packageCodeUpsales_id = data[i].id
                        params.packageCodeUpsales = data[i].id + "_" + value
                    }
                }
            }
            params.startDate = params.startDate ? dayjs(params.startDate, 'DD-MM-YYYY').format('DD/MM/YYYY') : '';
        }
        if(this.props.isNew) params.id = '';
        callApiModify(apiSecret, this.props.controller, params).then(response => {
        if (response.errors == null) {
            window.location.reload();
        } else{
            alert(response.errors[0].message)
        }}).catch(error => {
            alert(error)
        });
    }

    componentDidMount() {
        // các phần CKEDITOR của các controller viết trực tiếp ở đây
        // if (this.props.controller == "") {
        //     CKEDITOR.replace('content', {
        //         customConfig: '/assets/js/ckeditor_config.js'
        //     });
        // }
    }

render() {
    const { isModalVisible, onClose, optionsSelect} = this.props;
    const {item} = this.state;
    return (
        <Modal
            destroyOnClose={true}
            title= {<div style={{ fontSize: '25px' }}>{this.props.title}</div>}
            visible={isModalVisible}
            onCancel={onClose}
            footer={null}
            width={1150}
        >
        <div className='ant-modal-body-frist' style={{alignItems: 'center', justifyContent: 'center'}}>

            {this.props.items.map(v => {
                switch (v.type) {
                    case 'text':
                        return (
                            <div key={v.attribute}>
                                <label htmlFor={v.attribute}>{v.title}</label>
                                <Input
                                    style={{ width: '100%' }}
                                    id={v.attribute}
                                    value={item[v.attribute]}
                                    onChange={e => this.handleChange(`${v.attribute}`, e.target.value)}
                                />
                            </div>
                        );
                    case 'text-content':
                        return (
                            <div key={v.attribute}>
                                <label htmlFor={v.attribute}>{v.title}</label>
                                <TextArea
                                    rows={8}
                                    style={{ width: '100%' }}
                                    id={v.attribute}
                                    value={item[v.attribute]}
                                    onChange={e => this.handleChange(`${v.attribute}`, e.target.value)}
                                />
                            </div>
                        );
                    case 'select':
                        return (
                            <div key={v.attribute}>
                                <label htmlFor={v.attribute}>{v.title}</label>
                                <Select
                                    id={v.attribute}
                                    value={`${item[v.attribute]}`}
                                    onChange={value => this.handleChange(`${v.attribute}`, value)}
                                    style={{ width: '100%' }}
                                >
                                {Object.entries(optionsSelect).map(([code, title]) => (
                                    <Select.Option key={code} value={code}>
                                        {title}
                                    </Select.Option>
                                ))}
                                </Select>
                            </div>
                        );
                    case 'multiselect':
                        return (
                            <div key={v.attribute}>
                            <label htmlFor={v.attribute}>{v.title}</label>
                            <Select
                                mode="multiple"
                                id={v.attribute}
                                value={item[v.attribute]}
                                onChange={value => this.handleChange(`${v.attribute}`, value)}
                                style={{ width: '100%' }}
                            >
                                {Object.entries(optionsSelect).map(([code, title]) => (
                                <Select.Option key={code} value={code}>
                                    {title}
                                </Select.Option>
                                ))}
                            </Select>
                            </div>
                        );                          
                    case 'select1':
                        return (
                            <div key={v.attribute}>
                                <label htmlFor={v.attribute}>{v.title}</label>
                                <Select
                                    id={v.attribute}
                                    value={`${item[v.attribute]}`}
                                    onChange={value => this.handleChange(`${v.attribute}`, value)}
                                    style={{ width: '100%' }}
                                >
                                {Object.entries(v.options).map(([code, title]) => (
                                    <Select.Option key={code} value={code}>
                                        {title}
                                    </Select.Option>
                                ))}
                                </Select>
                            </div>
                        );
                    case 'date':
                        return (
                            <div key={v.attribute}>
                                <label htmlFor={v.attribute}>{v.title}</label>
                                <DatePicker
                                    id={v.attribute}
                                    value={item[v.attribute] ? dayjs(`${item[v.attribute]}`, 'DD-MM-YYYY') : ''}
                                    onChange={this.handleDateChange}
                                    style={{ width: '100%' }}
                                />
                            </div>
                        );
                    case 'ckeditor':
                        return(
                            <div>
                            <label htmlFor={v.attribute}>{v.title}</label>
                            <TextArea
                                id={v.attribute}
                                name={v.attribute}
                                value={this.state.item.attribute}
                                onChange={(e) => this.handleEditorChange(e)}
                            />
                            </div>
                        );
                    default:
                        return null;
                }
            })}
        </div>
        <div className='ant-modal-body-two'>
                <Button className = "btn btn-primary" onClick={this.handleSaveModal}>Lưu lại</Button>            
                <Button className = "btn btn-default" style={{ marginLeft: '15px'}} onClick={onClose}>Bỏ qua</Button>
        </div>

        </Modal>
    );
  }
}

export default PackageUpsalesModal;
