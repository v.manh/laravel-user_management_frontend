import React, { Component } from "react";
import logo from "../../assets/images/logo.png";
import styled from "styled-components";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import Home from "./LoggedIn.js";

export default class Main extends Component {
    constructor(props) {
        let boolean = false;
        // if (localStorage && localStorage.hasOwnProperty("sessionInfo")) {
        //     boolean = true;
        // }
        super(props);
        this.state = {
            isLoggedIn: true,
            data: null,
            username: "",
            password: "",
        };
    }

    handleChange = (event) => {
        this.setState({ [event.target.name]: event.target.value });
    };

    validateForm = () => {
        const { username, password } = this.state;
        if (username === "") {
            toast.error("Vui lòng nhập tài khoản");
            return false;
        } else if (password === "") {
            toast.error("Vui lòng nhập mật khẩu");
            return false;
        }
        return true;
    };

    handleSubmit = (event) => {
        event.preventDefault();
        if (this.validateForm()) {
            const { username, password } = this.state;
            if (username === "1" && password === "1") {
                this.setState({ isLoggedIn: true });
            } else {
                toast.error("Tên người dùng hoặc mật khẩu không chính xác");
            }
        }
    };

    render() {
        const { isLoggedIn } = this.state;

        if (isLoggedIn) {
            return <Home />;
        }

        return (
            <>
                <FormContainer>
                    <form onSubmit={this.handleSubmit}>
                        <div className="brand">
                            <img
                                src={logo}
                                alt="logo"
                                style={{ height: "25px" }}
                            />
                        </div>
                        <input
                            type="text"
                            placeholder="Tài khoản"
                            name="username"
                            onChange={this.handleChange}
                            min="3"
                        />
                        <input
                            type="password"
                            placeholder="Mật khẩu"
                            name="password"
                            onChange={this.handleChange}
                        />
                        <button type="submit">Đăng nhập</button>
                    </form>
                </FormContainer>
                <ToastContainer />
            </>
        );
    }
}

const FormContainer = styled.div`
    height: 100vh;
    width: 100vw;
    display: flex;
    flex-direction: column;
    justify-content: center;
    gap: 1rem;
    align-items: center;
    background-color: #f7f7f7;
    .brand {
        display: flex;
        align-items: center;
        gap: 1rem;
        justify-content: center;
        img {
        height: 5rem;
        }
        h3 {
        color: white;
        text-transform: uppercase;
        }
    }

    form {
        display: flex;
        flex-direction: column;
        gap: 2rem;
        background-color: #ffffff;
        border: 0.1rem solid #dedede;
        border-radius: 0.5rem;
        padding: 5rem;
    }

    input {
        background-color: #ffffff;
        padding: 1rem;
        border: 0.1rem solid #dedede;
        border-radius: 0.4rem;
        width: 86%;
        font-size: 1rem;
        &:focus {
        border: 0.1rem solid #997af0;
        outline: none;
        }
    }

    button {
        background-color: #65bd77;
        color: white;
        padding: 1rem 1.5rem;
        border: none;
        cursor: pointer;
        border-radius: 0.4rem;
        font-size: 0.8rem;
        text-transform: uppercase;
        &:hover {
        background-color: #4fac62#4e0eff;
        }
    }
};`;
