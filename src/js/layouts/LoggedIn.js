import React, { useState } from 'react';
import { Layout, Menu, Button } from 'antd';
import {
  MenuFoldOutlined,
  MenuUnfoldOutlined,
  UserOutlined,
  VideoCameraOutlined,
  UploadOutlined,
} from '@ant-design/icons';
import { BrowserRouter as Router, Routes, Route, Link } from 'react-router-dom';
import logo from '../../assets/images/logo.png';
import SearchPortrait from '../views/searchportrait/Content.js'
import Admin from '../views/admin/Content.js'
const { Header, Sider, Content } = Layout;
const { SubMenu } = Menu;
const About = () => <h1>About</h1>;
const Contact = () => <h1>Contact</h1>;

const LoggedIn = () => {
  const [collapsed, setCollapsed] = useState(false);

  const menuData = [
    {
      key: '1',
      icon: 'UserOutlined',
      label: 'Tra cứu',
      subItems: [
        {
          key: '1-1',
          label: 'Ảnh chân dung',
          link: "/searchportrait/detail"
        },
        {
          key: '1-2',
          label: 'sub-menu 3',
          subItems: [
            {
              key: '1-2-1',
              label: 'sub-menu 2.1',
            },
            {
              key: '1-2-2',
              label: 'sub-menu 2.2',
            },
          ],
        },
      ],
    },
    {
      key: '2',
      icon: 'UserOutlined',
      label: 'Quản trị viên',
      link: "/admin/list"
    },
    {
      key: '3',
      icon: 'UploadOutlined',
      label: 'nav 3',
    },
  ];

  const renderIcon = (icon) => {
    switch (icon) {
      case 'UserOutlined':
        return <UserOutlined />;
      case 'VideoCameraOutlined':
        return <VideoCameraOutlined />;
      case 'UploadOutlined':
        return <UploadOutlined />;
      default:
        return null;
    }
  };

  const renderMenuItems = (menuItems) => {
    return menuItems.map((item) => {
      if (item.subItems && item.subItems.length > 0) {
        return (
          <SubMenu key={item.key} icon={renderIcon(item.icon)} title={item.label}>
            {renderMenuItems(item.subItems)}
          </SubMenu>
        );
      } else {
        return (
          <Menu.Item key={item.key} icon={renderIcon(item.icon)}>
            <Link to={item.link}>{item.label}</Link>
          </Menu.Item>
        );
      }
    });
  };

  return (
    <Router>
      <Layout>
        <Sider trigger={null} collapsible collapsed={collapsed}>
          <div className="logo">
            <img src={logo} alt="Logo" />
          </div>
          <Menu theme="dark" mode="inline" defaultSelectedKeys={['1']}>
            {renderMenuItems(menuData)}
          </Menu>
        </Sider>
        <Layout>
          <Header style={{ padding: 0 }}>
            <Button
              type="text"
              icon={collapsed ? <MenuUnfoldOutlined /> : <MenuFoldOutlined />}
              onClick={() => setCollapsed(!collapsed)}
            />
          </Header>
          <Content style={{ margin: '24px 16px', padding: 24, minHeight: 280 }}>
            <Routes>
              <Route path="/" element={<About />} />
              <Route path="/searchportrait/detail" element={<SearchPortrait />} />
              <Route path="/admin/list" element={<Admin />} />
              <Route path="/2" element={<Contact />} />
            </Routes>
          </Content>
        </Layout>
      </Layout>
    </Router>
  );
};

export default LoggedIn;
