import React from "react";
import { Button } from "antd";
import Grid from "./Grid";
import "../../layouts/Layout.css";

const TITLE = "ảnh chân dung";

class Content extends React.Component {
  render() {
    return (
    <div className="content-container">
      <div className="title-container">
        <div className="caption">Tra cứu {TITLE}</div>
      </div>
      {/* <div className="button-container">
        <Button type="primary" className="add-button">Thêm {TITLE}</Button>
      </div> */}
      <div className="grid-container">
        <Grid />
      </div>
    </div>
    );
  }
}

export default Content;
