import React from "react";
import { Table, Image, Button, Input, Spin, Alert } from 'antd';
//import axios from "axios";

class Grid extends React.Component {
    constructor(props) {
        super(props);
        let sessionInfo = null;
        if (localStorage && localStorage.hasOwnProperty("sessionInfo")) {
            let value = localStorage.getItem("sessionInfo");
            if (value) sessionInfo = JSON.parse(value);
        }
        this.state = {
            sessionInfo: sessionInfo,
            loading: false,
            error: null,
            searchValue: '',
            convertData: null,
        };
    }

    handleSearchChange = (event) => {
        this.setState({ searchValue: event.target.value });
    };

    handleSearch = () => {
        this.handleSearchClick();
    };

    handleSearchClick = () => {
        const { searchValue, sessionInfo } = this.state;
        this.setState({ loading: true, error: null });

        // axios.post(webapiDomain + "/cms/searchportrait/detail",{ phone: searchValue },
        //     {
        //         headers: {
        //             apisecret: sessionInfo ? sessionInfo.apiSecret : null,
        //             "Content-Type": "application/x-www-form-urlencoded;charset=UTF-8",
        //         },
        //     }
        // ).then((response) => {
        //     switch (response.status) {
        //     case 200:
        //         return response.data;
        //     case 401:
        //         throw new Error("Session đã hết hạn!");
        //     case 501:
        //         throw new Error(
        //         "Tài khoản không có quyền thực hiện chức năng này. Vui lòng liên hệ với Admin để được giải quyết"
        //         );
        //     default:
        //         throw new Error("Xảy ra lỗi");
        //     }
        // }).then((data) => {
        //     const convertData = data.data.length ? data.data.map((e, i) => ({ ...e, stt: i + 1 })) : [];
        //     this.setState({ convertData: convertData, loading: false });
        // })
        // .catch((error) => {
        //     this.setState({ loading: false, error: error.message });
        //     console.error(error);
        // });
    };

    render() {
        const { convertData, loading} = this.state;
        const dataSource = convertData && Array.isArray(convertData) ? convertData.map((item, index) => ({
            key: index + 1,
            stt: index + 1,
            image: item.IMAGE || null,
        })) : [];

        const columns = [
        {
            title: 'STT',
            dataIndex: 'stt',
            key: 'stt',
        },
        {
            title: 'Ảnh',
            dataIndex: 'image',
            key: 'image',
            render: (image) => (
            <Image
                className="thumbnail"
                src={`data:image/jpeg;base64, ${image}`}
                alt="Ảnh"
                style={{ maxWidth: '100px', maxHeight: '100px' }}
            />
            ),
        }
        ];

        return (
        <div>
            <div style={{ display: 'flex', marginBottom: '15px' }}>
                <Input
                    value={this.state.searchValue}
                    onChange={this.handleSearchChange}
                    placeholder="Số điện thoại tra cứu"
                    style={{ width: '200px', marginRight: '10px' }}
                    onKeyDown={(e) => {
                        if (e.key === "Enter") {
                          this.handleSearch();
                        }
                    }}
                />
                <Button type="primary" onClick={this.handleSearchClick}>
                    Tìm kiếm
                </Button>
            </div>

            {loading ? (<Spin size="large"/>) : (
                <>
                    {convertData && convertData.length === 0 && (
                        <Alert message="Không tìm thấy dữ liệu" type="info" showIcon />
                    )}
                    {convertData && convertData.length > 0 && (
                        <Table columns={columns} bordered dataSource={dataSource} pagination={false} />
                    )}
                </>
            )}
        </div>
        );
    }
}

export default Grid;
