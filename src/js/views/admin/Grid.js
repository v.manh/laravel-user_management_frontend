import React from "react";
//import Search from "../../widgets/Search";
import { Space, Table, Tag, Pagination   } from 'antd';
//import Paging from "../../widgets/Paging";
import "../../helpers/Grid.css";
class Grid extends React.Component {
  // Phương thức xử lý sự kiện thay đổi trang và số trang trên bảng  
    render() {
        console.log("this.props.convertData",this.props.convertData)
        let columns = [
            {
                title: 'STT',
                dataIndex: 'stt',
                key: 'stt',
            },
            {
                title: 'Gói',
                dataIndex: 'packageCodeUse',
                key: 'packageCodeUse',
            },
            {
                title: 'Gói cước UpSales',
                dataIndex: 'packageCodeUpsales',
                key: 'packageCodeUpsales',
            },
            {
                title: 'DeepLink',
                dataIndex: 'deeplink',
                key: 'deeplink',
            },
            {
                title: 'Ngày bắt đầu',
                dataIndex: 'startDate',
                key: 'startDate',
            },
            {
                title: 'Nội dung',
                dataIndex: 'content',
                key: 'content',
            },
            {
                title: 'Trạng thái',
                dataIndex: 'statusText',
                key: 'statusText',
            },
            {
                title: 'Hành động',
                dataIndex: 'action',
                key: 'action',
                render: (_, record) => {
                    return (
                        <Space size="middle">
                            <button className="btn btn-default s-edit-btn sbtn1" onClick={() => this.props.modifyPackageUpsales(record)}>Sửa</button>
                            <button className="btn btn-default s-delete-btn sbtn2" onClick={() => this.props.deletePackageUpsales(record)}>Xóa</button>
                        </Space>
                    )
                },
            }
        ];
    return (
        <div>
            {/* <Search items={[
                {
                    attribute:'packageCodeUse',
                    title: 'Gói',
                    type:'text'
                }
            ]} /> */}

            <Table columns={columns} 
            bordered
            dataSource={this.props.convertData}
            pagination={false}
            />

            <div style={{ marginTop: '20PX' }}>
                {/* <Paging style={{ marginTop: '20px' }}/> */}
            </div>

            {/* <Pagination
                showSizeChanger
                defaultCurrent={1}
                total={24}
                pageSizeOptions={['3', '5', '10', '20']}
                onChange={this.props.onChange}
            /> */}
        </div>
    )
    }
}

export default Grid;