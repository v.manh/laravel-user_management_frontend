import React from "react";
import dayjs from "dayjs";
import Grid from "./Grid";
import { Button } from "antd";
import Modal from "../../helpers/Modal";
import customParseFormat from "dayjs/plugin/customParseFormat";
import { callApiDelete, callApiGetListUsers } from "../../helpers/api";
const TITLE = "Quản trị viên";
const APIURL = "/cms/packageupsales/list";
const CONTROLLER = "packageupsales";

dayjs.extend(customParseFormat);

class Content extends React.Component {
    _isMounted = false;

    constructor(props) {
        super(props);
        let sessionInfo = null;
        // if (localStorage && localStorage.hasOwnProperty("sessionInfo")) {
        //     let value = localStorage.getItem("sessionInfo");
        //     if (value) sessionInfo = JSON.parse(value);
        // }
        this.state = {
            isNew: false,
            timestamp: new Date().getTime(),
            data: null,
            sessionInfo: sessionInfo,
            error: null,
            packageTypes: null,
            item: {
                packageCodeUse: "",
                content: "",
                typePushUpsale: "",
                status: "",
                packageType: "",
                packageCodeUpsales_id: "",
                packageCodeUpsales: "",
                startDate: null,
            },
        };
    }

    loadData(idata) {
        if (this._isMounted) this.setState({ data: idata });
    }

    loadpackageTypes(ipackageTypes) {
        if (this._isMounted) this.setState({ packageTypes: ipackageTypes });
    }

    clearSession() {
        this.setState({ sessionInfo: null });
        localStorage.clear();
    }

    componentDidMount() {
        // let apiSecret = this.state.sessionInfo ? this.state.sessionInfo.apiSecret : null;
        // if (!apiSecret) alert("Không có apiSecret")

        this._isMounted = true;
        this.loadData(null);

        let dataType = {
            data: [
                {
                    id: 1,
                    title: "Gói Linh hoạt SD",
                    code: "SD",
                },
                {
                    id: 2,
                    title: "Gói Linh hoạt MD",
                    code: "MD",
                },
                {
                    id: 3,
                    title: "Gói Linh hoạt LD",
                    code: "LD",
                },
                {
                    id: 4,
                    title: "Gói Linh hoạt XD",
                    code: "XD",
                },
                {
                    id: 5,
                    title: "Gói cước linh hoạt SM",
                    code: "SM",
                },
            ],
            errors: null,
        };
        callApiGetListUsers().then((response) => {
            this.loadData(response.data.data.docs);
        });
        //this.loadData(data);
        this.loadpackageTypes(dataType);
    }

    componentWillUnmount() {
        this._isMounted = false;
    }

    handleOpenModal = (item) => {
        this.setState({ isModalVisible: true });
        const str = item.packageCodeUpsales;
        const parts = str.split("_");
        if (parts.length > 1) {
            const nextChar = parts[1];
            this.setState({
                item: {
                    ...item,
                    packageType: nextChar,
                },
            });
        }
    };

    handleCloseModal = () => {
        this.setState({
            isModalVisible: false,
            item: {
                packageCodeUse: "",
                content: "",
                typePushUpsale: "",
                status: "",
                packageType: "",
                packageCodeUpsales_id: "",
                packageCodeUpsales: "",
                startDate: null,
            },
        });
    };

    deletePackageUpsales = (item) => {
        if (!window.confirm("Bạn có muốn xoá không?")) {
            return;
        }
        let apiSecret = this.state.sessionInfo
            ? this.state.sessionInfo.apiSecret
            : null;
        if (!apiSecret) return false;
        this._isMounted = true;
        let params = {
            id: item.id,
        };
        // callApiDelete(apiSecret, CONTROLLER, params).then(response => {
        // if (response.errors == null) {
        //     window.location.reload();
        // } else{
        //     alert(response.errors[0].message)
        // }}).catch(error => {
        //     alert(error)
        // });
    };

    render() {
        const { data, sessionInfo, packageTypes, isModalVisible, item, isNew } =
            this.state;
        var packageTypesList = {};
        let convertData;

        // if (!sessionInfo) {
        //   window.location.href = '/#/user/login';
        //   return null;
        // }

        packageTypesList[""] = "Chọn gói Upsale";
        if (packageTypes && packageTypes.data) {
            // Check if packageTypes and packageTypes.data are defined
            packageTypes.data.forEach(function (element, index) {
                packageTypesList[element.code] = element.title;
            });
        }

        if (data) {
            convertData = data.data.map((e, i) => ({ ...e, stt: i + 1 }));
        }

        return (
            <div className="content-container">
                <div className="title-container">
                    <div className="caption">{TITLE}</div>
                </div>
                <div className="button-container">
                    <Button
                        type="primary"
                        className="add-button"
                        onClick={() =>
                            this.setState({
                                isModalVisible: true,
                                isNew: true,
                                item: item,
                            })
                        }
                    >
                        Thêm {TITLE}
                    </Button>
                </div>
                <div className="grid-container">
                    {isModalVisible && (
                        <Modal
                            isNew={isNew}
                            isModalVisible={isModalVisible}
                            onClose={this.handleCloseModal}
                            optionsSelect={packageTypesList}
                            controller={CONTROLLER}
                            title={
                                isNew
                                    ? "Thêm gói cước Upsales"
                                    : "Sửa gói cước Upsales"
                            }
                            item={item}
                            apiSecret={sessionInfo}
                            packageTypes={packageTypes}
                            items={[
                                {
                                    attribute: "packageCodeUse",
                                    title: "Mã gói sử dụng(Cách nhau bằng dấu ,)(*)",
                                    type: "text",
                                },
                                {
                                    attribute: "packageType",
                                    title: "Loại gói",
                                    type: "select",
                                },
                                {
                                    attribute: "startDate",
                                    title: "Ngày bắt đầu",
                                    type: "date",
                                },
                                {
                                    attribute: "content",
                                    title: "Nội dung",
                                    type: "text-content",
                                },
                                {
                                    attribute: "typePushUpsale",
                                    title: "Loại UpSale",
                                    type: "select1",
                                    options: {
                                        "": "Chọn loại",
                                        1: "Gửi SMS trước khi gói cước hết hạn hàng ngày",
                                        2: "Gửi Noti hàng ngày",
                                    },
                                },
                                {
                                    attribute: "status",
                                    title: "Trạng thái",
                                    type: "select1",
                                    options: {
                                        "": "Chọn trạng thái",
                                        0: "Chờ duyệt",
                                        1: "Duyệt",
                                        2: "Xóa",
                                    },
                                },
                            ]}
                        />
                    )}
                    <Grid
                        editUser={(item) => {
                            this.editUser(item);
                        }}
                        deleteUser={(item) => {
                            this.deleteUser(item);
                        }}
                        convertData={convertData}
                        currentPage={1}
                        getListUsers={this.getListUsers}
                        perpage={this.state.perpage}
                        totalItem={this.state.totalItem}
                        handleCloseModal={() => this.handleCloseModal()}
                    />
                </div>
            </div>
        );
    }
}

export default Content;
