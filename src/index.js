import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import Main from './js/layouts/Main';

ReactDOM.render(
  <React.StrictMode>
    <Main />
  </React.StrictMode>,
  document.getElementById('root')
);
